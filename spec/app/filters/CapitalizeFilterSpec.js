'use strict';

define(['app/filters'], function () {
    describe('Filter: Capitalize Filter', function () {
        var capitalize;
        beforeEach(function () {
            module('beerDay.filters');
            inject(function ($filter) {
                capitalize = $filter('Capitalize');
            });
        });

        it('Capitalizes a string of text', function () {
            expect(capitalize('this is a test')).toMatch('This Is A Test');
        });
    });
});
