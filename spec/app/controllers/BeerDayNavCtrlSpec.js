'use strict';
define(['app/controllers'], function () {

	describe('Controller: BeerDayNavCtrl', function () {

		var ctrl,
			scope,
            location;


		beforeEach(function () {
			module('beerDay.controllers');

			inject(function ($controller, $rootScope, $location) {
				scope = $rootScope.$new();

				ctrl = $controller(require('app/controllers/BeerDayNavCtrl'), {
					$scope: scope,
					navigation: [{label: 'Home', path: ''}]
				});

                location = $location;
			});
		});

		it('exposes appName on scope', function () {
            expect(scope.appName).toBe('beer-day');
        });

        it('check for currently selected navigation path', function () {
            scope.navButtons.forEach(function (button, index, array) {
                location.path(button.path);
                expect(scope.pathIsSelected('#/' + button.path)).toBe(true);
            });

            expect(scope.pathIsSelected('/fakepath')).toBe(false);
        });
	});


})
