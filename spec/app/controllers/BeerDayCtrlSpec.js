'use strict';
define(['app/controllers'], function () {

	describe('Controller: BeerDayCtrl', function () {

		var ctrl,
			scope,
			location;


		beforeEach(function () {
			module('beerDay.controllers');

			inject(function ($controller, $rootScope, $location) {
				scope = $rootScope.$new();

				ctrl = $controller(require('app/controllers/BeerDayCtrl'), {
					$scope: scope
				});

				location = $location;
			});
		});

		it('exposes appDisplayName on scope', function () {
			expect(scope.appDisplayName).toBe('Beer day');
		});

	});


})
