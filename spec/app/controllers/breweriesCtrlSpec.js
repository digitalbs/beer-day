define(["app/controllers"], function () {

    describe('Controller: BreweriesCtrl', function () {

        var ctrl,
            scope;


        beforeEach(function () {
            module('beerDay.controllers');

            inject(function ($controller, $rootScope) {
                scope = $rootScope.$new();

                ctrl = $controller(require("app/controllers/BreweriesCtrl"), {
                    $scope: scope
                });
            });
        });

        it('exposes the correct msg on scope', function () {
            expect(scope.msg).toBe("Welcome to the breweries section");
        });
    });


});