define(["app/controllers"], function () {

    describe('Controller: BeersCtrl', function () {

        var ctrl,
            scope;


        beforeEach(function () {
            module('beerDay.controllers');

            inject(function ($controller, $rootScope) {
                scope = $rootScope.$new();

                ctrl = $controller(require("app/controllers/BeersCtrl"), {
                    $scope: scope
                });
            });
        });

        it('exposes the correct msg on scope', function () {
            expect(scope.msg).toBe("Welcome to the beers section");
        });
    });


});