define(["app/providers"], function () {

    describe('Provider: search', function () {

        var search;


        beforeEach(function () {
            module('beerDay.providers');

            inject(function (_search_) {
                search = _search_;
            });
        });

        it('returns the name of the provider', function () {
            expect(search.sayName()).toBe("search");
        });
    });


});
