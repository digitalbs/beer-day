define(["app/providers"], function () {

    describe('Provider: beers', function () {

        var beers;


        beforeEach(function () {
            module('beerDay.providers');

            inject(function (_beers_) {
                beers = _beers_;
            });
        });

        it('returns the name of the provider', function () {
            expect(beers.sayName()).toBe("beers");
        });
    });


});
