define(["app/providers"], function () {

    describe('Provider: geo', function () {

        var geo;


        beforeEach(function () {
            module('beerDay.providers');

            inject(function (_geo_) {
                geo = _geo_;
            });
        });

        it('returns the name of the provider', function () {
            expect(geo.sayName()).toBe("geo");
        });
    });


});
