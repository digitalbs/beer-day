define(["app/providers"], function () {

    describe('Provider: brewries', function () {

        var brewries;


        beforeEach(function () {
            module('beerDay.providers');

            inject(function (_brewries_) {
                brewries = _brewries_;
            });
        });

        it('returns the name of the provider', function () {
            expect(brewries.sayName()).toBe("brewries");
        });
    });


});
