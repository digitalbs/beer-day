define(['app/filters/CapitalizeFilter'], function (CapitalizeFilter) {
	angular.module('beerDay.filters', [])
        .filter('Capitalize', CapitalizeFilter);
});
