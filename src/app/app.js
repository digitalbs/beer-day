define([
    "app/config",
    "app/templates",
    "app/controllers",
    "app/filters",
    "app/directives",
    "app/providers"
], function (config, templates) {
    /**
     * beerDay Module
     * @module beerDay
     */
    angular.module("beerDay", [
            "ngRoute",
            "ngResource",
            "ui.bootstrap",
            "ngAnimate",
            "beerDay.controllers",
            "beerDay.filters",
            "beerDay.directives",
            "beerDay.providers"
        ])
    
    /**
     * Sets up Angular Application settings (Routing, etc...)
     * @class config
     * @constructor
     * @param $routeProvider {Object} Angular provider that allows us to configure routes in the application
     * @param $locationProvider {Object} Angular provider that configures how the applications deep links paths are stored
     */
        .config(["$routeProvider", "$locationProvider",
            function ($routeProvider, $locationProvider) {

                $routeProvider.when("/", {
                    templateUrl: 'app/views/default.html',
                    controller: 'BeerDayCtrl'
                });

                $routeProvider.when("/beers", {
                    templateUrl: "app/views/beers.html",
                    controller: "BeersCtrl",
                    resolve: {
                        beers: function (Beers, $rootScope) {
                            $rootScope.isLoading = true;
                            return Beers.get({withBreweries:'Y'}).$promise;
                        }
                    }
                });
                $routeProvider.when("/beers/:id", {
                    templateUrl: "app/views/beerDetails.html",
                    controller: "BeerDetailsCtrl",
                    resolve: {
                        beer: function (Beers, $rootScope, $route) {
                            $rootScope.isLoading = true;
                            return Beers.getBeer({id: $route.current.params.id, withBreweries:'Y'}).$promise;
                        }
                    }
                });
                
                $routeProvider.when("/breweries", {
                    templateUrl: "app/views/breweries.html",
                    controller: "breweriesCtrl",
                    resolve: {
                        breweries: function(Geo, $rootScope) {
                            $rootScope.isLoading = true;
                            return Geo.get({lat: '32.7758', lng:'-96.7967', radius: 100}).$promise;
                        }
                    }
                });

                $routeProvider.when("/breweries/:id/beers", {
                    templateUrl: "app/views/beers.html",
                    controller: "BeersCtrl",
                    resolve: {
                        beers: function (Breweries, $route, $rootScope) {
                            $rootScope.isLoading = true;
                            return Breweries.get({id: $route.current.params.id, withBreweries:'Y'}).$promise;
                        }
                    }
                });

                $routeProvider.otherwise({
                    templateUrl: 'app/views/404.html',
                    controller: 'BeerDayCtrl'
                });

                $locationProvider.html5Mode(config.html5Routing);
            }
        ])
        .constant('navigation', config.navigation)
        
        .run(['$templateCache', templates]);
});
