define(['app/providers/beers','app/providers/search','app/providers/geo','app/providers/breweries'], function(beers,search,geo,breweries){
	angular.module('beerDay.providers', [])
    .factory('Beers', beers)
    .factory('Search',search)
    .factory('Geo',geo)
    .factory('Breweries',breweries);
});
