//default seed filter method
define([], function () {
    'use strict';
    return function () {
        return function (text) {
            return text.toLowerCase().replace( /\b./g, function (a){
                return a.toUpperCase();
            });
        };
    };
});