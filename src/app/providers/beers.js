'use strict';
define([], function () {
    /**
     * @type Factory
     * @module beerDay.factory
     * @class beers
     */

    function beers($resource) {

        var beers = $resource('http://localhost:3000/beers/:id', { id: '@id' }, {
            getBeer: {method: 'GET'}
        });

        return beers;
    }

    return ['$resource', beers];
});
