'use strict';
define([], function () {
    /**
     * @type Factory
     * @module beerDay.factory
     * @class brewries
     */

    function breweries($resource) {
        var breweries = $resource('http://localhost:3000/brewery/:id', { id: '@id' });

        return breweries;
    }

    return ['$resource', breweries];
});
