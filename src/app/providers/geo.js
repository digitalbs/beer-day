'use strict';
define([], function () {
    /**
     * @type Factory
     * @module beerDay.factory
     * @class geo
     */

    function geo($resource) {

        return $resource('http://localhost:3000/search/geosearch');

    }

    return ['$resource', geo];
});
