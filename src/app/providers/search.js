'use strict';
define([], function () {
    /**
     * @type Factory
     * @module beerDay.factory
     * @class search
     */

    function search($resource) {

        return $resource('http://localhost:3000/search');

    }

    return ['$resource', search];
});
