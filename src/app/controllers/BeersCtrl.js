define([], function () {
    /**
     * @module agilityX.controllers
     * @class BeersCtrl
     * @constructor
     * @param $scope {Object} Angular Scope object tied to the BeersCtrl Controller
     */
    function BeersCtrl($scope, $rootScope, $location, beers, Beers, Search) {
        /**
         * Default message for about page
         * @property msg
         * @type {string}
         */
        $rootScope.isLoading = false;
        $scope.paging = false;
        $scope.searching = false;

        var numPages = beers.numberOfPages,
            currPage = beers.currentPage,
            beers = beers.data;


        $scope.beers = beers;


        if(numPages > 1) {
            $scope.paging = true;
        }

        $scope.getBeer = function(id) {
            $location.url('/beers/' + id);
        }

        $scope.getMoreBeers = function () {

            currPage = currPage + 1;
            $scope.fetching = true;
            Beers.get({withBreweries: 'Y', p:currPage}).$promise.then(function (res) {
                beers = beers.concat(res.data);
                $scope.fetching = false;
                $scope.beers = beers;
            });
        }

        $scope.searchBeers = function () {
            $scope.searching = true;
            Search.get({q: $scope.search, type:'beer'}, function (res) {
                $scope.searching = false;

                if(res.data) {
                    $scope.beers = res.data;
                    $scope.noResults = false;
                } else {
                    $scope.beers = [];
                    $scope.noResults = "No results found";
                }

            });
        }

    }

    return ['$scope', '$rootScope', '$location', 'beers', 'Beers', 'Search', BeersCtrl];

});
