define([], function () {
    /**
     * @module agilityX.controllers
     * @class BeersCtrl
     * @constructor
     * @param $scope {Object} Angular Scope object tied to the BeersCtrl Controller
     */
    function BeerDetailsCtrl($scope, $window, $rootScope, beer) {
        /**
         * Default message for about page
         * @property msg
         * @type {string}
         */

        $rootScope.isLoading = false;
        $scope.beer = beer.data;
        $scope.goBack = function () {
            $window.history.back();
        }
    }

    return ['$scope', '$window', '$rootScope', 'beer', BeerDetailsCtrl];

});
