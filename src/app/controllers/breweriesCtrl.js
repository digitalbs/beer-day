define([], function () {
    /**
     * @module agilityX.controllers
     * @class BreweriesCtrl
     * @constructor
     * @param $scope {Object} Angular Scope object tied to the BreweriesCtrl Controller
     */
    function BreweriesCtrl($scope, $rootScope, $location, breweries, Geo) {
        /**
         * Default message for about page
         * @property msg
         * @type {string}
         */

        var numPages = breweries.numberofPages,
            currPage = breweries.currentPage,
            breweries = breweries.data,
            currPage;

        $rootScope.isLoading = false;
        $scope.breweries = breweries;

        if(numPages < 1) {
            $scope.showButton = false;
        }

        $scope.getBrewery = function(id) {
            $location.url('/breweries/' + id + '/beers');
        }

        $scope.getMoreBreweries = function () {
            currPage = currPage + 1;
            Geo.get({lat: '32.7758', lng:'-96.7967', radius: 50, p:currPage}).$promise.then(function (res) {
                breweries = breweries.concat(res.data);
                $scope.breweries = breweries;
            });
        }
    }

    return ['$scope', '$rootScope', '$location', 'breweries', 'Geo', BreweriesCtrl];

});
