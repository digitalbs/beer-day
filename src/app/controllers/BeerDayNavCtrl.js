define(["app/config"], function (appConfig) {
    /**
     * @module beerDay.controllers
     * @class beerDayNavCtrl
     * @constructor
     * @param $scope {Object} Angular Scope object tied to the beerDayNavCtrl Controller
     * @param $location {Object} Angular's version of window.location(parses url in address bar)
     */
    function BeerDayNavCtrl ($scope, $location, navigation) {
        /**
         * Name of the application
         * @property appName
         * @type {string}
         */
        $scope.appName = "Better Brew Day";
        $scope.navButtons = [];

        navigation.forEach(function(navEntry){
            $scope.navButtons.push(navEntry);
        });

        $scope.navPrefix = appConfig.urlPrefix + "/";

        /**
         * Method to determine if nav links are the active state
         * based on the tab/page you are on.
         * @method pathIsSelected
         * @param path {string} the path is passed in to compare the current location
         * @returns {boolean} This method returns a boolean on an ng-class directive in the index.html
         */
        $scope.pathIsSelected = function (path) {
            var location = "#" + $location.path();
            return path === location;
        };
    }



    return ['$scope','$location','navigation',BeerDayNavCtrl];

});
