define(["app/config"], function (appConfig) {
    /**
     * @module beerDay.controllers
     * @class beerDayCtrl
     * @constructor
     * @param $scope {Object} Angular Scope object tied to the beerDayCtrl Controller
     * @param $location {Object} Angular's version of window.location(parses url in address bar)
     */
    function BeerDayCtrl ($scope, $rootScope) {
        $scope.appDisplayName = "Beer day";
        $scope.currentTime = new Date();

        //make this a service
        $rootScope.isLoading = false;
    }

    return ['$scope', '$rootScope', BeerDayCtrl];

});
