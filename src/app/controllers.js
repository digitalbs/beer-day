define([
    'app/controllers/BeerDayCtrl',
    'app/controllers/BeerDayNavCtrl',
    "app/controllers/BeersCtrl",
    "app/controllers/BeerDetailsCtrl",
    "app/controllers/breweriesCtrl"
], function (BeerDayCtrl, BeerDayNavCtrl, BeersCtrl, BeerDetailsCtrl, breweriesCtrl) {
    /**
     * @module agilityX.controllers
     */
	angular.module('beerDay.controllers', [])
	.controller('BeerDayNavCtrl', BeerDayNavCtrl)
    .controller("BeersCtrl", BeersCtrl)
    .controller('BeerDayCtrl', BeerDayCtrl)
    
    .controller("breweriesCtrl", breweriesCtrl)
    .controller('BeerDetailsCtrl', BeerDetailsCtrl);

});
